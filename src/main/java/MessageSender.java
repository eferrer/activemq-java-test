import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

public class MessageSender {

    // default broker URL is : tcp://localhost:61616"
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    // Name of the queue we will receive messages from
    private static final String SUBJECT = "testTopic";

    public static void main(String[] args) throws JMSException {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination destination = session.createQueue(SUBJECT);

        MessageProducer producer = session.createProducer(destination);

        TextMessage message = session.createTextMessage("{\"id\":3,\"name\":\"Julio\",\"lastName\":\"Carrera\"}");

        producer.send(message);
        System.out.println("Sent message");

        connection.close();
    }
}
